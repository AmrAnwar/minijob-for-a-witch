# Minijob for a witch(Minijob für eine Hexe)
<div align="center">
  <img src="https://i.ibb.co/mCBNj8r/148e0f94-03cc-420f-8022-1cfd47a865e5.jpg"/>
</div>
is a RPG children game based on unity Creator RPG Kit. 

- this game is a university project for the course(Game Design) at otto von Geurike university.


# Assets and sources we used:
- Fonts: 
  - [AR Darling](https://fontzone.net/font-details/ar-darling)

- Animation:
  - [Mais](https://www.fondation-louisbonduelle.org/en/vegetable/sweet-corn/)
  - [Hexe Bild](https://opengameart.org/content/witch-0)
  - [Seil](https://www.vexels.com/png-svg/preview/242012/circular-rope-knot-color-stroke)
  - [Cow](https://opengameart.org/content/livestock-0)
- unity Kit: 
  - [Creator Kit RPG](https://learn.unity.com/project/creator-kit-rpg?uv=2020.3)
- Music:  
  - [Menu/Spiel music](https://www.fesliyanstudios.com/royalty-free-music/downloads-c/scary-horror-music/8)

